load("@local//:triplet.bzl", LOCAL = "TRIPLET")
load(":TripletInfo.bzl", "TripletInfo")

visibility("//toolchain/...")

TRIPLETS = (
    LOCAL,
    TripletInfo("arm64-linux-gnu"),
    TripletInfo("amd64-linux-gnu"),
    TripletInfo("arm64-linux-musl"),
    TripletInfo("amd64-linux-musl"),
    TripletInfo("arm64-macos-darwin"),
    TripletInfo("amd64-macos-darwin"),
    TripletInfo("arm64-windows-ucrt"),
    TripletInfo("amd64-windows-ucrt"),
)
