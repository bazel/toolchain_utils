load("@bazel_skylib//lib:types.bzl", "types")
load("//toolchain:VersionedInfo.bzl", "VersionedInfo")
load("//toolchain:split.bzl", "split")

visibility("//toolchain/...")

def init(value):
    """
    Initializes a `CoupletInfo` provider.

    Processes `value` into the constituent parts.

    Args:
      value: A machine couplet which can be kebab-separated such as `aarch64-linux.6.13`

    Returns:
      A mapping of keywords for the `couplet_info` raw constructor.
    """
    if not types.is_string(value):
        fail("`CoupletInfo.value` must be a `str`: {}".format(value))

    cpu, vendor, os = split(value, "-", {
        2: lambda c, o: (c, None, o),
        3: lambda c, v, o: (c, v, o),
    })

    os = VersionedInfo(os)

    constraints = [
        "cpu:{}".format(cpu),
        "os:{}".format(os.kind),
    ]

    if os.version:
        constraints.append("os/{}:{}".format(os.kind, os.version.value))

    return {
        "value": value,
        "cpu": cpu,
        "vendor": vendor,
        "os": os,
        "constraints": tuple([
            Label("//toolchain/constraint/{}".format(c))
            for c in constraints
        ]),
    }

CoupletInfo, couplet_info = provider(
    "A machine couplet. Has the associated compatible Bazel constraints.",
    fields = ("value", "cpu", "vendor", "os", "constraints"),
    init = init,
)
