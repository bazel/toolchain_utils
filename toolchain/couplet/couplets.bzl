load("@local//:triplet.bzl", LOCAL = "TRIPLET")
load(":CoupletInfo.bzl", "CoupletInfo")

visibility("//toolchain/...")

COUPLETS = (
    CoupletInfo("{}-{}".format(LOCAL.cpu, LOCAL.os.value)),
    CoupletInfo("arm64-linux"),
    CoupletInfo("amd64-linux"),
    CoupletInfo("arm64-windows"),
    CoupletInfo("amd64-windows"),
    CoupletInfo("arm64-macos"),
    CoupletInfo("amd64-macos"),
)
