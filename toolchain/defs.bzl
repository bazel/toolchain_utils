load("//toolchain/couplet:couplets.bzl", _COUPLETS = "COUPLETS")
load("//toolchain/triplet:triplets.bzl", _TRIPLETS = "TRIPLETS")

visibility("public")

TOOLCHAIN_COUPLETS = _COUPLETS
TOOLCHAIN_TRIPLETS = _TRIPLETS
